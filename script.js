$(document).ready(function() {
  const $container = $("#container");
  $.getJSON("http://localhost:3000/players").then(function(players) {
    players.forEach(function(player) {
    	console.log(`${player.name}`);
      let $newPlayer = $("<div>", {
        html: `
            <input id=name_${player.id} value="${player.name}">
            <input id=score_${player.id} value=${player.score}>
            <button id=plus_${player.id} class="plus">+</button>
            <button id=minus_${player.id} class="minus">-</button>
            <button id=save_${player.id} class="save">Save</button>
            <button id=delete_${player.id} class="delete">Delete</button>
        `,
        "data-id": `${player.id}`
      });
      $container.append($newPlayer);
    });
  });

  $("#new-player-form").on("submit", function(e) {
    e.preventDefault();
    const name = $("#name").val();
    const score = $("#score").val();
    const email = $("#email").val();
    $.post("http://localhost:3000/players",{ name, score,email }).then(function(player) {
      let $newPlayer = $("<div>", {
        html: `
            <input id=name_${player.id} value=${player.name}>
            <input id=score_${player.id} value=${player.score}>
            <button id=plus_${player.id} class="plus">+</button>
            <button id=minus_${player.id} class="minus">-</button>
            <button id=save_${player.id} class="save">Save</button>
            <button id=delete_${player.id} class="delete">Delete</button>
        `,
        "data-id": `${player.id}`
      });
      $container.append($newPlayer);
      $("#new-player-form").trigger("reset");
      setTimeout(function(){ window.location.reload(); }, 1000);
      // window.location.reload();

    });
  });

  $container.on("click", ".delete", function(e) {
    e.preventDefault();
    const id = $(e.target)
      .parent()
      .data("id");
    const type = $
      .ajax({
        method: "DELETE",
        url: `http://localhost:3000/players/${id}`
      })
      .then(function() {
        $(e.target)
          .parent()
          .remove();
          $('#message_div').text("Deleted Successfully");
      });
  });

  $container.on("click", ".save", function(e) {
    e.preventDefault();
    const id = e.currentTarget.id.split("_")[1];
    const name=$("#name_"+id)[0]["value"];
    const score=$("#score_"+id)[0]["value"];
    const type = $
      .ajax({
        method: "PATCH",
        data: {name, score },
        url: `http://localhost:3000/players/${id}`
      })
      .then(function() {
      	console.log('Updated successfully');
      	$('#message_div').text("Updated Successfully");
      	setTimeout(function(){ location.reload(); }, 1000);
      });
  });

  $container.on("click", ".plus", function(e) {
    e.preventDefault();
    const id = e.currentTarget.id.split("_")[1];
    const score=parseInt($("#score_"+id)[0]["value"])+1;
    $('#score_'+id).val(score);
  });

  $container.on("click", ".minus", function(e) {
    e.preventDefault();
    const id = e.currentTarget.id.split("_")[1];
    const score=parseInt($("#score_"+id)[0]["value"])-1;
    $('#score_'+id).val(score);
  });

});