 players-app-frontend

 This is the front end of players app which gives you interface to add players and update the information of existing players

 To run this ropository 

Clone the code to your local system 
	
Install http server by running this command in terminal npm install -g http-server
	
Run this command in terminal to start the http server http-server -p 3001 
	
Navigate to http://localhost:3001/ to see the UI of the application ensure that your backend server is running on port 3000 before running this front end

![ImgName](https://bitbucket.org/coderhere/players-app-frontend/raw/master/screenshot.png) 

